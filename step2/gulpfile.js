var gulp        = require('gulp');
var sass        = require('gulp-sass');
//group file to template
var concat = require('gulp-concat');
//min css
var cssmin		= require('gulp-cssmin');
var template = require('gulp-template-html');
var rename = require("gulp-rename");
const gulpTemplator = require('gulp-template-html');
var browserSync = require('browser-sync').create();
gulp.task('browserSync', function() {
    browserSync.init({
      server: {
        baseDir: 'src'
      },
    })
  })
//sass developer
gulp.task('sassdev', () => {
	return gulp .src('src/assets/sass/*.scss')
		   		.pipe(sass().on('error', sass.logError))
		   		//.pipe(cssmin())/* lenh min css co the bo qua*/
		   		.pipe(gulp.dest('src/assets/css'))
                   //.pipe(reload({stream:true}));
                //    .pipe(browserSync.reload({
                //     stream: true
                // }))
                   
});
gulp.task('sass:watch', function () {
    gulp.watch('src/assets/sass/*.scss', gulp.series (['sassdev']));
 });

//sass product
gulp.task('sass', () => {
	return gulp .src('src/assets/sass/*.scss')
		   		.pipe(sass().on('error', sass.logError))
		   		.pipe(cssmin())/* lenh min css co the bo qua*/
		   		.pipe(gulp.dest('public/assets/css'))
		   		//.pipe(reload({stream:true}));
});
gulp.task('copyimg', () => {
	return gulp .src('src/assets/image/*.*')
		   		.pipe(gulp.dest('public/assets/image'))
		   		//.pipe(reload({stream:true}));
});
gulp.task('mergehtml', function() {
  return gulp.src('src/html/*.html')
    .pipe(concat('index.html'))
    .pipe(gulp.dest('src/'));
});

// var gulp = require('gulp');
// gulp.task('mergehtml', function(){
// 	return gulp.src('src/html/**/*.html')
// 	.pipe(concat("temp.html"))
// 	.pipe(gulp.dest('src/'));
// });


gulp.task('template', function () {
	// const glob = require('glob');
	// const fileArray = glob.sync('src/html/*.html');
	// for(let i =0,len=fileArray.length; i<len;i++){
	// 	let filename=fileArray[i].replace('src/','');
		
		return gulp.src('src/html/*.html')
		      .pipe(template('src/index.html'))
		    //   .pipe(rename(function (path) {
			//     // Returns a completely new object, make sure you return all keys needed!
			//     return {
			//       dirname: path.dirname + "",
			//       basename:filename,
			//       extname: ""
			//     };
			//   }))
		      .pipe(gulp.dest('public'))
	// }
});
